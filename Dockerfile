FROM registry.gitlab.com/rocket-team.com/docker/magento-php:latest

RUN apk add --no-cache nginx \
    && mkdir -p /run/nginx \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

COPY magento.conf /etc/nginx/conf.d/default.conf

COPY docker-entrypoint /usr/local/bin/docker-entrypoint

EXPOSE 80

ENTRYPOINT docker-entrypoint